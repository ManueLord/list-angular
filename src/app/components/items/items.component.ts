import { Component, OnInit } from '@angular/core';
import { Item } from '../../models/item'

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  items: Item[] = [];
  result: number = 0;

  constructor() { }

  ngOnInit(): void {
    this.items = [
      {
        id: 0,
        title: 'apple',
        price: 10.5,
        quantity: 4,
        completed: false
      },
      {
        id: 1,
        title: 'bread',
        price: 3.5,
        quantity: 8,  
        completed: true
      },
    ];
    this.getResult();
  }

  deleteItem(item:Item){
    this.items = this.items.filter(x => x.id !== item.id);
    this.getResult();
  }

  toggleItem(item:Item){
    this.getResult();
  }

  getResult(){
    this.result = this.items.filter(x => !x.completed).map(x => x.quantity * x.price).reduce((acc, item) => acc += item, 0);
  }
}
